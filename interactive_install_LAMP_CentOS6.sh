#!/bin/bash

PUBLIC_IP=$(wget http://ipinfo.io/ip -qO -)

pwgen () {
tr -cd '[:alnum:]' < /dev/urandom | fold -w12 | head -n1
}


clear

echo '------------------------'
echo 'Gathering information'
echo '------------------------'
read -p "Enter your root domain without www (e.g. mydomain.com): " MYDOMAIN
read -p "SFTP user name (e.g. sftpuser): " SFTPUSER
VHOSTSPATH="/var/www/vhosts"
SFTPPATH="$VHOSTSPATH/$MYDOMAIN"
DOCROOT="$SFTPPATH/public_html/"
echo "Default path for $SFTPUSER will be $DOCROOT"

echo "Please press any key to continue..."
read voidvar

clear

# Pre-creating path, in case does not exists
# and add a basic php test page
mkdir -p $DOCROOT
[ -e $DOCROOT/index.php ] || echo '<?php print "<h1>This is a test page</h1>"; ?>' >> $DOCROOT/index.php


SFTPPASS=$(pwgen)

echo "Installing and configuring packages..."
yum -y -q install httpd php php-pecl-apc php-mcrypt php-xml php-pear php-gd php-cli php-soap php-pdo php-mysql mysql mysql-server
chkconfig mysqld on
chkconfig httpd on

# Setting up vhost
mkdir -p /etc/httpd/vhost.d/
grep -q -F 'Include vhost.d/*.conf' /etc/httpd/conf/httpd.conf || echo "Include vhost.d/*.conf" >> /etc/httpd/conf/httpd.conf

cat <<EOF > /etc/httpd/vhost.d/$MYDOMAIN.conf
<VirtualHost *:80>
        ServerName $MYDOMAIN
        ServerAlias www.$MYDOMAIN
        DocumentRoot $DOCROOT

        # Force HTTPS when loading the page
        #RewriteEngine On
        #RewriteCond %{HTTPS} off
        #RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}

        <Directory $DOCROOT>
                Options -Indexes +FollowSymLinks -MultiViews
                AllowOverride All
        </Directory>

        CustomLog /var/log/httpd/$MYDOMAIN-access.log combined
        ErrorLog /var/log/httpd/$MYDOMAIN-error.log

        # Possible values include: debug, info, notice, warn, error, crit,
        # alert, emerg.
        LogLevel warn
</VirtualHost>

#<VirtualHost *:443>
#        ServerName $MYDOMAIN
#        ServerAlias www.$MYDOMAIN
#        DocumentRoot $DOCROOT
#        <Directory $DOCROOT>
#                Options -Indexes +FollowSymLinks -MultiViews
#                AllowOverride All
#        </Directory>
#
#        CustomLog /var/log/httpd/$MYDOMAIN-ssl-access.log combined
#        ErrorLog /var/log/httpd/$MYDOMAIN-ssl-error.log
#
#        # Possible values include: debug, info, notice, warn, error, crit,
#        # alert, emerg.
#        LogLevel warn
#
#        SSLEngine on
#        SSLCertificateFile    /etc/pki/tls/certs/$MYDOMAIN.crt
#        SSLCertificateKeyFile /etc/pki/tls/private/$MYDOMAIN.key
#        SSLCertificateChainFile /etc/pki/tls/private/$MYDOMAIN.ca.crt
#
#        <FilesMatch "\.(cgi|shtml|phtml|php)$">
#                SSLOptions +StdEnvVars
#        </FilesMatch>
#
#        BrowserMatch "MSIE [2-6]"                 nokeepalive ssl-unclean-shutdown                 downgrade-1.0 force-response-1.0
#        BrowserMatch "MSIE [17-9]" ssl-unclean-shutdown
#</VirtualHost>
EOF

service httpd start
service mysqld start

# Setting up MySQL

MYSQLADMINPASS=$(pwgen)
MYSQLADMIN=root
MYSQLHOST=localhost

/usr/bin/mysqladmin -u $MYSQLADMIN password $MYSQLADMINPASS


# Set up DATABASE
DBNAME=$(echo "${MYDOMAIN//[!0-9a-zA-Z]/}" | cut -c1-8)$RANDOM
DBUSER=$(pwgen)
DBPASS=$(pwgen)
mysql -u $MYSQLADMIN -h $MYSQLHOST -p$MYSQLADMINPASS -e "CREATE DATABASE $DBNAME"
mysql -u $MYSQLADMIN -h $MYSQLHOST -p$MYSQLADMINPASS -e "GRANT ALL PRIVILEGES ON $DBNAME.* to $DBUSER IDENTIFIED BY '$DBPASS'"
mysql -u $MYSQLADMIN -h $MYSQLHOST -p$MYSQLADMINPASS -e "GRANT ALL PRIVILEGES ON $DBNAME.* to '$DBUSER'@'$MYSQLHOST' IDENTIFIED BY '$DBPASS'"




# Creating /root/.my.cnf
cat > /root/.my.cnf <<EOF
[client]
user=root
password=$MYSQLADMINPASS
EOF


BKP="/home/restore/$(date +%Y%m%d%H%M%S)_backup"
echo "Backing up important files in $BKP in case of restore"
mkdir -p $BKP
chown root:root $BKP
chmod 0600 $BKP
cp /etc/group $BKP/etc-group
cp /etc/passwd $BKP/etc-passwd
cp /etc/shadow $BKP/etc-shadow
cp /etc/ssh/sshd_config $BKP/etc-ssh-sshd_config


echo 'Setting up SFTP user...'
grep sftponly /etc/group > /dev/null || groupadd sftponly
useradd -d $SFTPPATH -s /bin/false -G sftponly $SFTPUSER

# Setting up SSH
sed -i 's/^Subsystem sftp.*$/Subsystem sftp internal-sftp/' /etc/ssh/sshd_config

grep "Match Group sftponly" /etc/ssh/sshd_config > /dev/null || echo "
Match Group sftponly
   ChrootDirectory %h
   X11Forwarding no
   AllowTCPForwarding no
   ForceCommand internal-sftp
" >> /etc/ssh/sshd_config

service sshd restart


# Setting up path and permissions
chown root:root $SFTPPATH
chmod -R 755 $SFTPPATH
mkdir -p $DOCROOT
chown -R $SFTPUSER:apache $DOCROOT
chmod 775 $DOCROOT

# Set SFTP user password
SFTPPASS=$(pwgen)
echo "$SFTPPASS" | passwd --stdin $SFTPUSER

clear

# REPORT
echo "
-----------
 SFTP user
-----------
username: $SFTPUSER
password: $SFTPPASS
IP: $PUBLIC_IP
path: $SFTPPATH
writing dir: $DOCROOT

-----------
   MySQL
-----------
admin user: $MYSQLADMIN
root password: $MYSQLADMINPASS

-----------
 Database
-----------
database: $DBNAME
username: $DBUSER
password: $DBPASS

-----------
  Apache
-----------
Test site: curl -iH'Host: $MYDOMAIN' http://$PUBLIC_IP/
If not working, verify the firewall.
"
