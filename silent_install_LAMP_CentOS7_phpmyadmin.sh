#!/bin/bash

########################################################
# Script to install Apache, PHP and MariaDB on CentOS7 #
########################################################

PUBLIC_IP=$(wget http://ipinfo.io/ip -qO -)

pwgen () {
tr -cd '[:alnum:]' < /dev/urandom | fold -w12 | head -n1
}

MYSQLADMIN=root
MYSQLPASS=$(pwgen)
PHPMYADMUSR="dbadmin"
PHPMYADMPASS=$(pwgen)

echo "Installing and configuring Apache and PHPMyAdmin"
# For WEB
yum -y -q install httpd php php-pecl-apc php-mcrypt php-xml php-pear php-gd php-cli php-soap php-pdo php-mysql phpmyadmin


# Setup PHPMyADMIN
htpasswd -bm -c /etc/httpd/.htpasswdfile $PHPMYADMUSR $PHPMYADMPASS

mv /etc/httpd/conf.d/phpMyAdmin.conf{,.ORIG}

cat <<'EOF' > /etc/httpd/conf.d/phpMyAdmin.conf

Alias /phpMyAdmin /usr/share/phpMyAdmin
Alias /phpmyadmin /usr/share/phpMyAdmin

<Directory /usr/share/phpMyAdmin/>
AuthUserFile /etc/httpd/.htpasswdfile
AuthName Restricted
AuthType Basic
require valid-user
</Directory>

<Directory /usr/share/phpMyAdmin/setup/>
   <IfModule mod_authz_core.c>
     # Apache 2.4
     <RequireAny>
       Require ip 127.0.0.1
       Require ip ::1
     </RequireAny>
   </IfModule>
   <IfModule !mod_authz_core.c>
     # Apache 2.2
     Order Deny,Allow
     Deny from All
     Allow from 127.0.0.1
     Allow from ::1
   </IfModule>
</Directory>

<Directory /usr/share/phpMyAdmin/libraries/>
    Order Deny,Allow
    Deny from All
    Allow from None
</Directory>

<Directory /usr/share/phpMyAdmin/setup/lib/>
    Order Deny,Allow
    Deny from All
    Allow from None
</Directory>

<Directory /usr/share/phpMyAdmin/setup/frames/>
    Order Deny,Allow
    Deny from All
    Allow from None
</Directory>
EOF

# Remove few annoying warnings
# (remove end of file, add line and re-add the end of file)
sed -i 's/\?>//' /etc/phpMyAdmin/config.inc.php
echo -e "\$cfg['ServerLibraryDifference_DisableWarning'] = TRUE;\n\$cfg['VersionCheck'] = FALSE;\n?>" >> /etc/phpMyAdmin/config.inc.php

systemctl enable httpd.service
systemctl start httpd.service


echo "Setting up firewall to allow HTTP/HTTPS"
# Opening HTTP/HTTPS
firewall-cmd --permanent --zone=public --add-service=http
firewall-cmd --permanent --zone=public --add-service=https
systemctl restart  firewalld.service


echo "Installing and configuring the Database"
# Install and setup MySQL
yum -y -q install mariadb-server mariadb
systemctl enable mariadb.service
systemctl start mariadb.service

# Setting up MySQL
/usr/bin/mysqladmin -u $MYSQLADMIN password $MYSQLPASS

# Creating /root/.my.cnf
cat > /root/.my.cnf <<EOF
[client]
user=$MYSQLADMIN
password=$MYSQLPASS
EOF

clear

echo "
-----------
   MySQL
-----------
admin user: $MYSQLADMIN
root password: $MYSQLADMINPASS

------------
 PHPMyAdmin
------------
username: $PHPMYADMUSR
password: $PHPMYADMPASS

"
