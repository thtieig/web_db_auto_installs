#!/bin/bash

##########################################
# Script to install Wordpress on CentOS7 #
##########################################

# This script will install the latest available version of
# Wordpress on a server with Apache+PHP-FPM+MariaDB.
# It will setup 'wp' SFTP user jailed within the DocRoot.
# It also creates a PHP-FPM pool with this SFTP user as 
# owner.

PUBLIC_IP=$(wget http://ipinfo.io/ip -qO -)

pwgen () {
tr -cd '[:alnum:]' < /dev/urandom | fold -w12 | head -n1
}

clear

echo '------------------------'
echo 'Gathering information'
echo '------------------------'
read -p "Enter your Wordpress domain name without www (e.g. mydomain.com): " MYDOMAIN
echo -e "\nYour Wordpress site is $MYDOMAIN\n"

echo "Please press any key to continue... or CTRL+C to abort."
read voidvar

clear

# Variables
SFTPUSER="wp"
VHOSTSPATH="/var/www/vhosts"
SFTPPATH="$VHOSTSPATH/$MYDOMAIN"
DOCROOT="$SFTPPATH/public_html/"

# Pre-setting credentials
SFTPPASS=$(pwgen)
MYSQLPASS=$(pwgen)
PHPMYADMUSR="dbadmin"
PHPMYADMPASS=$(pwgen)
MYSQLADMINPASS=$(pwgen)
MYSQLADMIN=root
MYSQLHOST=localhost
DBNAME=$(echo "${MYDOMAIN//[!0-9a-zA-Z]/}" | cut -c1-8)$RANDOM
DBUSER="wp$(pwgen)"
DBPASS=$(pwgen)

BKP="/home/restore/$(date +%Y%m%d%H%M%S)_backup"
echo "Backing up important files in $BKP in case of restore"
mkdir -p $BKP
chown root:root $BKP
chmod 0600 $BKP
cp /etc/group $BKP/etc-group
cp /etc/passwd $BKP/etc-passwd
cp /etc/shadow $BKP/etc-shadow
cp /etc/ssh/sshd_config $BKP/etc-ssh-sshd_config


echo 'Setting up SFTP user for PHP-FPM pool'
mkdir -p $VHOSTSPATH
grep sftponly /etc/group > /dev/null || groupadd sftponly
useradd -d $SFTPPATH -s /bin/false -G sftponly $SFTPUSER

# Setting up SSH
sed -i 's/^Subsystem sftp.*$/Subsystem sftp internal-sftp/' /etc/ssh/sshd_config

grep "Match Group sftponly" /etc/ssh/sshd_config > /dev/null || echo "
Match Group sftponly
   ChrootDirectory %h
   X11Forwarding no
   AllowTCPForwarding no
   ForceCommand internal-sftp
" >> /etc/ssh/sshd_config && systemctl restart  sshd.service

# Pre-creating path, in case does not exists
mkdir -p $DOCROOT

# Install Wordpress
wget -qnc http://wordpress.org/latest.tar.gz
tar xzf latest.tar.gz -C /tmp
mv /tmp/wordpress/* $DOCROOT/ > /dev/null
rm -rf /tmp/wordpress > /dev/null

# Pre set wp-config
cp $DOCROOT/wp-config-sample.php $DOCROOT/wp-config.php
sed -i "s/^define('DB_USER'.*$/define('DB_USER', '$DBUSER');/" $DOCROOT/wp-config.php
sed -i "s/^define('DB_HOST'.*$/define('DB_HOST', '$MYSQLHOST');/" $DOCROOT/wp-config.php
sed -i "s/^define('DB_NAME'.*$/define('DB_NAME', '$DBNAME');/" $DOCROOT/wp-config.php
sed -i "s/^define('DB_PASSWORD'.*$/define('DB_PASSWORD', '$DBPASS');/" $DOCROOT/wp-config.php

# Fixing permissions
chown root:root $SFTPPATH
chmod -R 755 $SFTPPATH
chown -R $SFTPUSER:$SFTPUSER $DOCROOT
chmod 775 $DOCROOT

# Set SFTP user password
echo "$SFTPPASS" | passwd --stdin $SFTPUSER


echo "Installing and configuring Apache, PHP-FPM"
# WEB
yum -y -q install httpd php php-pecl-apc php-mcrypt php-xml php-pear php-gd php-cli php-soap php-pdo php-mysql php-fpm

# Setting up vhost
mkdir -p /etc/httpd/vhost.d/
grep -q -F 'vhost.d/*.conf' /etc/httpd/conf/httpd.conf || echo "IncludeOptional vhost.d/*.conf" >> /etc/httpd/conf/httpd.conf


# creating $MYDOMAIN vhost
cat <<EOF > /etc/httpd/vhost.d/$MYDOMAIN.conf
<VirtualHost *:80>
        ServerName $MYDOMAIN
        ServerAlias www.$MYDOMAIN

        DocumentRoot $DOCROOT
        <Directory $DOCROOT>
                require all granted
                Options Indexes
                AllowOverride All
        </Directory>

        ErrorLog "logs/$MYDOMAIN-error_log"
        CustomLog "logs/$MYDOMAIN-access_log" combined env=!forwarded
        CustomLog "logs/$MYDOMAIN-access_log" proxy env=forwarded

        # Configure proxy connector
        <Proxy "unix:/dev/shm/$MYDOMAIN-php-fpm.sock|fcgi://php-fpm-$MYDOMAIN">
                # we must declare a parameter in here (doesn't matter which) or it'll not register the proxy ahead of time
                ProxySet disablereuse=off
        </Proxy>
        #
        # Redirect to the proxy connector
        <FilesMatch \.php$>
                SetHandler proxy:fcgi://php-fpm-$MYDOMAIN
        </FilesMatch>


        <Directory $DOCROOT/wp-content/uploads>
            # Important for security, prevents someone from
            # uploading a malicious .htaccess
            AllowOverride None

            SetHandler none
            SetHandler default-handler

            Options -ExecCGI
            php_flag engine off
            RemoveHandler .cgi .php .php3 .php4 .php5 .phtml .pl .py .pyc .pyo
            <Files *>
                    AllowOverride None

                    SetHandler none
                    SetHandler default-handler

                    Options -ExecCGI
                    php_flag engine off
                    RemoveHandler .cgi .php .php3 .php4 .php5 .phtml .pl .py .pyc .pyo
            </Files>
        </Directory>
</VirtualHost>
EOF


# Creating php session path for php-fpm pool
mkdir -p /var/lib/php/session_$MYDOMAIN
chown $SFTPUSER:$SFTPUSER /var/lib/php/session_$MYDOMAIN
chmod 770 /var/lib/php/session_$MYDOMAIN


# Configuring php-fpm

# Creating $MYDOMAIN pool
cat <<EOF > /etc/php-fpm.d/$MYDOMAIN.conf
[$MYDOMAIN]
listen = /dev/shm/$MYDOMAIN-php-fpm.sock
user = $SFTPUSER
group = $SFTPUSER
listen.owner = $SFTPUSER
listen.group = apache
listen.mode = 0666
pm = dynamic
pm.max_children = 35
pm.start_servers = 5
pm.min_spare_servers = 5
pm.max_spare_servers = 25
slowlog = /var/log/php-fpm/$MYDOMAIN-slow.log
php_admin_value[error_log] = /var/log/php-fpm/$MYDOMAIN-error.log
php_admin_flag[log_errors] = on
php_value[session.save_handler] = files
php_value[session.save_path] = /var/lib/php/session/$MYDOMAIN
EOF

# Fixing logging issue
touch /var/log/php-fpm/$MYDOMAIN-slow.log
touch /var/log/php-fpm/$MYDOMAIN-error.log
chown $SFTPUSER:$SFTPUSER /var/log/php-fpm/$MYDOMAIN*log
chmod 755 /var/log/php-fpm/


# Start Apache and PHP-FPM
systemctl enable httpd.service php-fpm.service
systemctl start httpd.service php-fpm.service


echo "Setting up firewall to allow HTTP/HTTPS"
# Opening HTTP/HTTPS
firewall-cmd --permanent --zone=public --add-service=http
firewall-cmd --permanent --zone=public --add-service=https
systemctl restart  firewalld.service


echo "Installing and configuring the Database"
# Install and setup MySQL
yum -y -q install mariadb-server mariadb
systemctl enable mariadb.service
systemctl start mariadb.service


# Setting up MySQL
/usr/bin/mysqladmin -u $MYSQLADMIN password $MYSQLADMINPASS

# Set up DATABASE
mysql -u $MYSQLADMIN -h $MYSQLHOST -p$MYSQLADMINPASS -e "CREATE DATABASE $DBNAME"
mysql -u $MYSQLADMIN -h $MYSQLHOST -p$MYSQLADMINPASS -e "GRANT ALL PRIVILEGES ON $DBNAME.* to $DBUSER IDENTIFIED BY '$DBPASS'"
mysql -u $MYSQLADMIN -h $MYSQLHOST -p$MYSQLADMINPASS -e "GRANT ALL PRIVILEGES ON $DBNAME.* to '$DBUSER'@'$MYSQLHOST' IDENTIFIED BY '$DBPASS'"


# Creating /root/.my.cnf
cat > /root/.my.cnf <<EOF
[client]
user=$MYSQLADMIN
password=$MYSQLADMINPASS
EOF

BKP="/home/restore/$(date +%Y%m%d%H%M%S)_backup"
echo "Backing up important files in $BKP in case of restore"
mkdir -p $BKP
chown root:root $BKP
chmod 0600 $BKP
cp /etc/group $BKP/etc-group
cp /etc/passwd $BKP/etc-passwd
cp /etc/shadow $BKP/etc-shadow
cp /etc/ssh/sshd_config $BKP/etc-ssh-sshd_config


echo 'Setting up SFTP user...'
grep sftponly /etc/group > /dev/null || groupadd sftponly
useradd -d $SFTPPATH -s /bin/false -G sftponly $SFTPUSER

# Setting up SSH
sed -i 's/^Subsystem sftp.*$/Subsystem sftp internal-sftp/' /etc/ssh/sshd_config

grep "Match Group sftponly" /etc/ssh/sshd_config > /dev/null || echo "
Match Group sftponly
   ChrootDirectory %h
   X11Forwarding no
   AllowTCPForwarding no
   ForceCommand internal-sftp
" >> /etc/ssh/sshd_config

service sshd restart


# Setting up path and permissions
chown root:root $SFTPPATH
chmod -R 755 $SFTPPATH
mkdir -p $DOCROOT
chown -R $SFTPUSER:apache $DOCROOT
chmod 775 $DOCROOT

# Set SFTP user password
echo "$SFTPPASS" | passwd --stdin $SFTPUSER

clear


# REPORT
echo "
-----------
 SFTP user
-----------
username: $SFTPUSER
password: $SFTPPASS
IP: $PUBLIC_IP
path: $SFTPPATH
writing dir: $DOCROOT

-----------
   MySQL
-----------
admin user: $MYSQLADMIN
root password: $MYSQLADMINPASS

-----------
 Database
-----------
database: $DBNAME
username: $DBUSER
password: $DBPASS

-----------
  Summary
-----------
Please change your hosts file to point to $PUBLIC_IP or point your DNS to it.
Alternatively, you can test using this command:
curl -iH'Host: $MYDOMAIN' http://$PUBLIC_IP/
"

