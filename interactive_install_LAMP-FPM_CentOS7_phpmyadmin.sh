#!/bin/bash

############################################################
# Script to install Apache+PHP-FPM and  MariaDB on CentOS7 #
############################################################

# This script will setup an SFTP user that will be jailed
# within the DocRoot of the website.
# It creates also a PHP-FPM pool with this SFTP user as 
# owner.
# DB and PHPMyAdmin pre-configured as well.
# NOTE: it creates a default vhost and a default php-fpm
# pool to allow PHPMyAdmin to run and log separated from
# the default domain

PUBLIC_IP=$(wget http://ipinfo.io/ip -qO -)

pwgen () {
tr -cd '[:alnum:]' < /dev/urandom | fold -w12 | head -n1
}

clear

echo '------------------------'
echo 'Gathering information'
echo '------------------------'
read -p "Enter your root domain without www (e.g. mydomain.com): " MYDOMAIN
read -p "SFTP user name (e.g. sftpuser): " SFTPUSER
VHOSTSPATH="/var/www/vhosts"
SFTPPATH="$VHOSTSPATH/$MYDOMAIN"
DOCROOT="$SFTPPATH/public_html/"
echo -e "\nDefault path for $SFTPUSER will be $DOCROOT\n"

echo "Please press any key to continue... or CTRL+C to abort."
read voidvar

clear

# Pre-setting credentials
SFTPPASS=$(pwgen)
MYSQLPASS=$(pwgen)
PHPMYADMUSR="dbadmin"
PHPMYADMPASS=$(pwgen)
MYSQLADMINPASS=$(pwgen)
MYSQLADMIN=root
MYSQLHOST=localhost
DBNAME=$(echo "${MYDOMAIN//[!0-9a-zA-Z]/}" | cut -c1-8)$RANDOM
DBUSER=$(pwgen)
DBPASS=$(pwgen)

BKP="/home/restore/$(date +%Y%m%d%H%M%S)_backup"
echo "Backing up important files in $BKP in case of restore"
mkdir -p $BKP
chown root:root $BKP
chmod 0600 $BKP
cp /etc/group $BKP/etc-group
cp /etc/passwd $BKP/etc-passwd
cp /etc/shadow $BKP/etc-shadow
cp /etc/ssh/sshd_config $BKP/etc-ssh-sshd_config


echo 'Setting up SFTP user for PHP-FPM pool'
grep sftponly /etc/group > /dev/null || groupadd sftponly
useradd -d $SFTPPATH -s /bin/false -G sftponly $SFTPUSER

# Setting up SSH
sed -i 's/^Subsystem sftp.*$/Subsystem sftp internal-sftp/' /etc/ssh/sshd_config

grep "Match Group sftponly" /etc/ssh/sshd_config > /dev/null || echo "
Match Group sftponly
   ChrootDirectory %h
   X11Forwarding no
   AllowTCPForwarding no
   ForceCommand internal-sftp
" >> /etc/ssh/sshd_config && systemctl restart  sshd.service


# Pre-creating path, in case does not exists
# and add a basic php test page
mkdir -p $DOCROOT
[ -e $DOCROOT/index.php ] || echo "<?php print \"<h1>$MYDOMAIN is setup</h1>\"; ?>" >> $DOCROOT/index.php


# Fixing permissions
chown root:root $SFTPPATH
chmod -R 755 $SFTPPATH
chown -R $SFTPUSER:$SFTPUSER $DOCROOT
chmod 775 $DOCROOT

# Adding extra test page to check if the SFTP user is actually used by PHP-FPM
cd $DOCROOT
cat > check.php <<EOF
<?php
  echo '<br><br>This website is running as: <b>' . exec('/usr/bin/whoami') . '</b>';
  echo '<br><br>From path: <b><i>' . getcwd() . '</i></b><br><br>';
  echo '<br><b><font size="5" color="red">DELETE THIS ONCE TESTED!</font></b>' . "\n";
?>
EOF


# Set SFTP user password
echo "$SFTPPASS" | passwd --stdin $SFTPUSER



echo "Installing and configuring Apache, PHP-FPM and PHPMyAdmin"
# WEB
yum -y -q install httpd php php-pecl-apc php-mcrypt php-xml php-pear php-gd php-cli php-soap php-pdo php-mysql php-fpm phpmyadmin


# Setting up vhost
mkdir -p /etc/httpd/vhost.d/
grep -q -F 'vhost.d/*.conf' /etc/httpd/conf/httpd.conf || echo "IncludeOptional vhost.d/*.conf" >> /etc/httpd/conf/httpd.conf

# creating default vhost to allow phpMyAdmin separated by the main domain
cat <<EOF > /etc/httpd/vhost.d/000-default.conf
# Default vhost
# Required for phpMyAdmin
<VirtualHost *:80>
    DocumentRoot "/var/www/html"
    ServerName webserver
    CustomLog "logs/default-access.log" combined env=!forwarded
    ErrorLog "logs/default-error.log"
</VirtualHost>
EOF

# Adding basic default message
echo "<br>Oh yes...<br>Apache seems <b>working</b>"'!'" :-)" > /var/www/html/index.html

# creating $MYDOMAIN vhost
cat <<EOF > /etc/httpd/vhost.d/$MYDOMAIN.conf
<VirtualHost *:80>
        ServerName $MYDOMAIN
        ServerAlias www.$MYDOMAIN

        DocumentRoot $DOCROOT
        <Directory $DOCROOT>
                require all granted
                Options Indexes
                AllowOverride All
        </Directory>

        ErrorLog "logs/$MYDOMAIN-error_log"
        CustomLog "logs/$MYDOMAIN-access_log" combined env=!forwarded
        CustomLog "logs/$MYDOMAIN-access_log" proxy env=forwarded

        # Configure proxy connector
        <Proxy "unix:/dev/shm/$MYDOMAIN-php-fpm.sock|fcgi://php-fpm-$MYDOMAIN">
                # we must declare a parameter in here (doesn't matter which) or it'll not register the proxy ahead of time
                ProxySet disablereuse=off
        </Proxy>
        #
        # Redirect to the proxy connector
        <FilesMatch \.php$>
                SetHandler proxy:fcgi://php-fpm-$MYDOMAIN
        </FilesMatch>
</VirtualHost>
EOF


# Creating php session path for php-fpm pool
mkdir -p /var/lib/php/session_$MYDOMAIN
chown $SFTPUSER:$SFTPUSER /var/lib/php/session_$MYDOMAIN
chmod 770 /var/lib/php/session_$MYDOMAIN


# Configuring php-fpm

# Creating $MYDOMAIN pool
cat <<EOF > /etc/php-fpm.d/$MYDOMAIN.conf
[$MYDOMAIN]
listen = /dev/shm/$MYDOMAIN-php-fpm.sock
user = $SFTPUSER
group = $SFTPUSER
listen.owner = $SFTPUSER
listen.group = apache
listen.mode = 0666
pm = dynamic
pm.max_children = 35
pm.start_servers = 5
pm.min_spare_servers = 5
pm.max_spare_servers = 25
slowlog = /var/log/php-fpm/$MYDOMAIN-slow.log
php_admin_value[error_log] = /var/log/php-fpm/$MYDOMAIN-error.log
php_admin_flag[log_errors] = on
php_value[session.save_handler] = files
php_value[session.save_path] = /var/lib/php/session/$MYDOMAIN
EOF

# Fixing logging issue
touch /var/log/php-fpm/$MYDOMAIN-slow.log
touch /var/log/php-fpm/$MYDOMAIN-error.log
chown $SFTPUSER:$SFTPUSER /var/log/php-fpm/$MYDOMAIN*log
chmod 755 /var/log/php-fpm/


echo "Configuring PHPMyAdmin"

# Backing up the default pool (if exists) and create a new one
mv /etc/php-fpm.d/www.conf{,.ORIG}  > /dev/null 2>&1
cat <<EOF > /etc/php-fpm.d/www.conf
[www]
listen = /dev/shm/www-php-fpm.sock
user = apache
group = apache
listen.owner = apache
listen.group = apache
listen.mode = 0666
pm = dynamic
pm.max_children = 35
pm.start_servers = 5
pm.min_spare_servers = 5
pm.max_spare_servers = 25
slowlog = /var/log/php-fpm/www-slow.log
php_admin_value[error_log] = /var/log/php-fpm/www-error.log
php_admin_flag[log_errors] = on
php_value[session.save_handler] = files
php_value[session.save_path] = /var/lib/php/session
EOF

# Fixing logging issue
touch /var/log/php-fpm/www-slow.log
touch /var/log/php-fpm/www-error.log
chown apache:apache /var/log/php-fpm/www*log
chmod 755 /var/log/php-fpm/


# Setup PHPMyADMIN
htpasswd -bm -c /etc/httpd/.htpasswdfile $PHPMYADMUSR $PHPMYADMPASS

mv /etc/httpd/conf.d/phpMyAdmin.conf{,.ORIG}

cat <<'EOF' > /etc/httpd/conf.d/phpMyAdmin.conf

Alias /phpMyAdmin /usr/share/phpMyAdmin
Alias /phpmyadmin /usr/share/phpMyAdmin

<Directory /usr/share/phpMyAdmin/>
AuthUserFile /etc/httpd/.htpasswdfile
AuthName Restricted
AuthType Basic
require valid-user
</Directory>

<Directory /usr/share/phpMyAdmin/setup/>
   <IfModule mod_authz_core.c>
     # Apache 2.4
     <RequireAny>
       Require ip 127.0.0.1
       Require ip ::1
     </RequireAny>
   </IfModule>
   <IfModule !mod_authz_core.c>
     # Apache 2.2
     Order Deny,Allow
     Deny from All
     Allow from 127.0.0.1
     Allow from ::1
   </IfModule>
</Directory>

<Directory /usr/share/phpMyAdmin/libraries/>
    Order Deny,Allow
    Deny from All
    Allow from None
</Directory>

<Directory /usr/share/phpMyAdmin/setup/lib/>
    Order Deny,Allow
    Deny from All
    Allow from None
</Directory>

<Directory /usr/share/phpMyAdmin/setup/frames/>
    Order Deny,Allow
    Deny from All
    Allow from None
</Directory>

# Configure proxy connector
<Proxy "unix:/dev/shm/www-php-fpm.sock|fcgi://php-fpm-www">
    # we must declare a parameter in here (doesn't matter which) or it'll not register the proxy ahead of time
    ProxySet disablereuse=off
</Proxy>
    # Redirect to the proxy connector
<FilesMatch \.php$>
    SetHandler proxy:fcgi://php-fpm-www
</FilesMatch>
EOF

# Remove few annoying warnings
# (remove end of file, add line and re-add the end of file)
sed -i 's/\?>//' /etc/phpMyAdmin/config.inc.php
echo -e "\$cfg['ServerLibraryDifference_DisableWarning'] = TRUE;\n\$cfg['VersionCheck'] = FALSE;\n?>" >> /etc/phpMyAdmin/config.inc.php


# Start Apache and PHP-FPM
systemctl enable httpd.service php-fpm.service
systemctl start httpd.service php-fpm.service


echo "Setting up firewall to allow HTTP/HTTPS"
# Opening HTTP/HTTPS
firewall-cmd --permanent --zone=public --add-service=http
firewall-cmd --permanent --zone=public --add-service=https
systemctl restart  firewalld.service


echo "Installing and configuring the Database"
# Install and setup MySQL
yum -y -q install mariadb-server mariadb
systemctl enable mariadb.service
systemctl start mariadb.service


# Setting up MySQL
/usr/bin/mysqladmin -u $MYSQLADMIN password $MYSQLADMINPASS

# Set up DATABASE
mysql -u $MYSQLADMIN -h $MYSQLHOST -p$MYSQLADMINPASS -e "CREATE DATABASE $DBNAME"
mysql -u $MYSQLADMIN -h $MYSQLHOST -p$MYSQLADMINPASS -e "GRANT ALL PRIVILEGES ON $DBNAME.* to $DBUSER IDENTIFIED BY '$DBPASS'"
mysql -u $MYSQLADMIN -h $MYSQLHOST -p$MYSQLADMINPASS -e "GRANT ALL PRIVILEGES ON $DBNAME.* to '$DBUSER'@'$MYSQLHOST' IDENTIFIED BY '$DBPASS'"


# Creating /root/.my.cnf
cat > /root/.my.cnf <<EOF
[client]
user=$MYSQLADMIN
password=$MYSQLADMINPASS
EOF

BKP="/home/restore/$(date +%Y%m%d%H%M%S)_backup"
echo "Backing up important files in $BKP in case of restore"
mkdir -p $BKP
chown root:root $BKP
chmod 0600 $BKP
cp /etc/group $BKP/etc-group
cp /etc/passwd $BKP/etc-passwd
cp /etc/shadow $BKP/etc-shadow
cp /etc/ssh/sshd_config $BKP/etc-ssh-sshd_config


echo 'Setting up SFTP user...'
grep sftponly /etc/group > /dev/null || groupadd sftponly
useradd -d $SFTPPATH -s /bin/false -G sftponly $SFTPUSER

# Setting up SSH
sed -i 's/^Subsystem sftp.*$/Subsystem sftp internal-sftp/' /etc/ssh/sshd_config

grep "Match Group sftponly" /etc/ssh/sshd_config > /dev/null || echo "
Match Group sftponly
   ChrootDirectory %h
   X11Forwarding no
   AllowTCPForwarding no
   ForceCommand internal-sftp
" >> /etc/ssh/sshd_config

service sshd restart


# Setting up path and permissions
chown root:root $SFTPPATH
chmod -R 755 $SFTPPATH
mkdir -p $DOCROOT
chown -R $SFTPUSER:apache $DOCROOT
chmod 775 $DOCROOT

# Set SFTP user password
echo "$SFTPPASS" | passwd --stdin $SFTPUSER

clear


# REPORT
echo "
-----------
 SFTP user
-----------
username: $SFTPUSER
password: $SFTPPASS
IP: $PUBLIC_IP
path: $SFTPPATH
writing dir: $DOCROOT

-----------
   MySQL
-----------
admin user: $MYSQLADMIN
root password: $MYSQLADMINPASS

-----------
 Database
-----------
database: $DBNAME
username: $DBUSER
password: $DBPASS

------------
 PHPMyAdmin
------------
username: $PHPMYADMUSR
password: $PHPMYADMPASS
URL: http://$PUBLIC_IP/phpmyadmin

-----------
  Apache
-----------
Test site: 
curl -iH'Host: $MYDOMAIN' http://$PUBLIC_IP/
curl -iH'Host: $MYDOMAIN' http://$PUBLIC_IP/check.php

"

